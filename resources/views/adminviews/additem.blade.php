@extends('layouts.app')
@section('content')
<h1 class="text-center">Add Item Form</h1>
<div class="container">
	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/additem" method="POST" enctype="multipart/form-data">
				@csrf

				<div class="form-group">
					<label for="name">Item Name</label>
					<input type="text" name="name" class="form-control" value="">		
				</div>

				<div class="form-group">
					<label for="price">Item Price</label>
					<input type="text" name="price" class="form-control">		
				</div>

				<div class="form-group">
					<label for="description">Description</label>
					<input type="text" name="description" class="form-control">		
				</div>

				<div class="form-group">
					<label for="imgPath">Image Path</label>
					<input type="file" name="imgPath" class="form-control">		
				</div>

				<div class="form-group">
					<label for="category_id">Category</label>
					<select type="select" name="category_id" class="form-control">
					@foreach($categories as $indiv_category)
						<option class="form-control" value="{{$indiv_category->id}}">{{$indiv_category->name}}</option>
					@endforeach
					</select>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-success">Add Item</button>
				</div>
				
			</form>
		</div>
	</div>
</div>
@endsection