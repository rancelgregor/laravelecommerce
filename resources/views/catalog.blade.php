@extends('layouts.app')
@section('content')

<h1 class="text-center">Catalog</h1>
@if(Session::has('message'))
	<h4>{{Session::get("message")}}</h4>
@endif

<div class="container">
	<div class="row bg-secondary">
		@foreach($items as $indiv_item)
		<div class="col-lg-4 my-3 bg-warning">
		<img src="{{$indiv_item->imgPath}}" height="400px" class="card-img-top">
			<div class="card">
				<div class="card-body">
						<h3 class="card-title">{{$indiv_item->name}}</h3>
						<p class="card-text">Price: {{$indiv_item->price}}</p>
						<p class="card-text">Description: {{$indiv_item->description}}</p>
						<p class="card-text">Category: {{$indiv_item->category->name}}</p>
				<div class="card-footer d-flex">
					<form action="/deleteitem/{{$indiv_item->id}}" method="POST">
						@csrf
						@method('DELETE')
						<button type="submit" class="btn btn-danger">DELETE</button>
					</form>
					<form action="/edititem/{{$indiv_item->id}}" method="POST">
						@csrf
						@method('PATCH')
						<a href="/edititem/{{$indiv_item->id}}" class="btn btn-success">Edit</a>
					</form>
				</div>
				<div class="card-footer">
					<form action="/addtocart/{{$indiv_item->id}}" method="POST">
						@csrf
						<input type="number" name="quantity" class="form-control" value="1">
						<button class="btn btn-primary" type="submit">Add to Cart</button>
					</form>
				</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
@endsection