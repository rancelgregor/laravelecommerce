<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use\App\Item;
use\App\Category;
use\App\Order;
use Auth;
use Session;



class ItemController extends Controller
{
    //
    public function generateCard(){
    	$items = Item::all();

    	return view('/catalog', compact('items'));
    }

    public function create(){
    	$categories = Category::all();

    	return view('adminviews.additem', compact('categories'));
    }

    public function store(Request $req){
    	// validate
    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required|numeric",//required at the same time numeric
    		"category_id" => "required",
    		"imgPath" => "required|image|mimes:jpeg,jpg,png,gif,tiff,tif,webp,bitmap|max:2048");


    	$this->validate($req,$rules);//validate ko yung request gamit yung restrictions
    	// dd($req);

    	//capture
    	$newItem = new Item;
    	$newItem->name = $req->name;
    	$newItem->description = $req->description;
    	$newItem->price = $req->price;
    	$newItem->category_id = $req->category_id;


    	//image handling
    	$image = $req->file('imgPath');
    	//We'll rename the image
    	$image_name = time() . "." . $image-> getClientOriginalExtension();

    	$destination = 'images/';//corresponds to the public images directory

    	$image->move($destination, $image_name);

    	$newItem->imgPath = $destination.$image_name;
    	//save
    	$newItem->save();
    	//redirect


    	Session::flash("message", "$newItem->name has been added");
    	
  		return redirect('/catalog');


    }

    public function destroy($id){
    	$itemToDelete = Item::find($id);
    	$itemToDelete->delete();
    	return redirect('/catalog');
    }

    public function editMe($id){
    	$item = Item::find($id);
    	$categories = Category::all();
    	return view('userviews.edititem', compact('item','categories'));
    }

    public function updateMe($id, Request $req){
    	$item = Item::find($id);
    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required|numeric",
    		"category_id" => "required",
    		"imgPath" => "image|mimes:jpeg,jpg,png,gif,tiff,tif,bitmap,webp"
    		);

    	$this->validate($req, $rules);

    	$item->name = $req->name;
    	$item->description = $req->description;
    	$item->price = $req->price;
    	$item->category_id = $req->category_id;

    	if($req->file('imgPath') != null){
    		$image = $req->file('imgPath');
    		$image_name = time(). "." . $image->getClientOriginalExtension();
    		$destination = "images/";
    		$image->move($destination, $image_name);
    		$item->imgPath = $destination.$image_name;
    	}
    	$item->save();
    	Session::flash('message', "$item->name has been updated");
    	return redirect('/catalog');
    }
    public function addToCart($id, Request $req){
    	//Check if there is an existing session
    	
    	if(Session::has('cart')){
    		$cart = Session::get('cart');

    	}else{
    		$cart = [];
    	}
    	//Check if this is the first time we will add an item to our cart
    	if(isset($car[$id])){
    		$cart[$id] += $req->quantity;

    	}else{
    		$cart[$id] = $req->quantity;
    	}

    	dd($cart);

    	//kapag updated na si cart..  ibabalik na sa session
    	Session::put("cart", $cart);


    	//Flash a message
    	// $item = Item::find(
    	//Find the item
    	//Use the name and quantity in your flash message
    	//Ex: 2 of Item Name successfully added to cart
    	$item = Item::find($id);
    	Session::flash("message", "$req->quantity of $item->name successfully added to cart");




    	return redirect()->back();
    }

    public function showCart(){

	    //we will create a new array containing Item name, price, quantity and subtotal
	    //Let's initialize and empty array
	    $items = [];
	    $total = 0;

	    if(Session::has('cart')){
	    	$cart = Session::get('cart');
	    	foreach($cart as $itemId => $quantity){
	    		$item = Item::find($itemId);
	    		$item->quantity = $quantity;
	    		$item->subtotal = $item->price * $quantity;
	    		$items[] = $item;
	    		$total += $item->subtotal;
	    	}
	    }
    	return view('userviews.cart', compact('items','total'));
    }
}
